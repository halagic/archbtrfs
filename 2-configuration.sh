#!/bin/bash

#   ____             __ _                       _   _             
#  / ___|___  _ __  / _(_) __ _ _   _ _ __ __ _| |_(_) ___  _ __  
# | |   / _ \| '_ \| |_| |/ _` | | | | '__/ _` | __| |/ _ \| '_ \ 
# | |__| (_) | | | |  _| | (_| | |_| | | | (_| | |_| | (_) | | | |
#  \____\___/|_| |_|_| |_|\__, |\__,_|_|  \__,_|\__|_|\___/|_| |_|
#                         |___/                                   
# ------------------------------------------------------
clear
keyboardlayout="de-latin1"
zoneinfo="Europe/Berlin"
hostname="arch"
username="moke"

# ------------------------------------------------------
# Set System Time
# ------------------------------------------------------
ln -sf /usr/share/zoneinfo/$zoneinfo /etc/localtime
hwclock --systohc

# ------------------------------------------------------
# Update reflector
# ------------------------------------------------------
echo ""
reflector --verbose -c "Germany" -p https -a 3 -l 20 -f 10 --sort rate --save /etc/pacman.d/mirrorlist
echo ""
cat /etc/pacman.d/mirrorlist
sleep 5

# Comment out the HOOKS= line in mkinitcpio.conf if it's not already commented
sed -i '/^HOOKS=/s/^/#/' /etc/mkinitcpio.conf

# modifications on makepakg.conf
sed -i 's/^#MAKEFLAGS="-j2"/MAKEFLAGS="-j10"/' /etc/makepkg.conf
sed -i '/^CFLAGS=/,/^.*/ {s/-march=[^ ]*//; s/-mtune=[^ ]*//; s/-fno-plt//; s/-fexceptions//; s/-Wp,-D_FORTIFY_SOURCE=2//; s/-Wformat//; s/-Werror=format-security//; s/-fstack-clash-protection//; s/-fcf-protection//; s/$/ -march=skylake  -O2 -pipe/}' /etc/makepkg.conf

# ------------------------------------------------------
# Synchronize mirrors
# ------------------------------------------------------
clear
pacman -Syy
# ------------------------------------------------------
# Install Packages
# ------------------------------------------------------
pacman -S --needed --noconfirm grub grub-btrfs efibootmgr os-prober dialog networkmanager network-manager-applet avahi dnsmasq nss-mdns openbsd-netcat inetutils dnsutils ipset mtools nfs-utils dosfstools ntfs-3g xdg-user-dirs xdg-utils gvfs gvfs-smb alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack acpid acpi acpi_call  sof-firmware zip unzip unrar unace p7zip bat htop neofetch duf flatpak xorg xorg-xinit xf86-video-amdgpu

# Uncomment the HOOKS= line in mkinitcpio.conf
sed -i '/^# HOOKS=/s/^# //' /etc/mkinitcpio.conf
mkinitcpio -p linux
# ------------------------------------------------------
# set lang utf8 US
# ------------------------------------------------------
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

# ------------------------------------------------------
# Set Keyboard
# ------------------------------------------------------
echo "FONT=ter-v28b" >> /etc/vconsole.conf
echo "KEYMAP=$keyboardlayout" >> /etc/vconsole.conf

# ------------------------------------------------------
# Set hostname and localhost
# ------------------------------------------------------
echo "$hostname" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 $hostname.localdomain $hostname" >> /etc/hosts
clear

# ------------------------------------------------------
# Set Root Password
# ------------------------------------------------------
echo "Set root password"
passwd root

# ------------------------------------------------------
# Add User
# ------------------------------------------------------
echo "Add user $username"
useradd -m -G wheel $username
passwd $username
clear
# ------------------------------------------------------
# Enable Services
# ------------------------------------------------------
systemctl enable NetworkManager
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable acpid

# ------------------------------------------------------
# Grub installation
# ------------------------------------------------------
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB --removable
grub-mkconfig -o /boot/grub/grub.cfg

# ------------------------------------------------------
# Add btrfs and setfont to mkinitcpio
# ------------------------------------------------------
# Before: BINARIES=()
# After:  BINARIES=(btrfs setfont)
sed -i 's/BINARIES=()/BINARIES=(btrfs setfont)/g' /etc/mkinitcpio.conf
mkinitcpio -p linux

# ------------------------------------------------------
# Add user to wheel
# ------------------------------------------------------
# Uncomment the %wheel line in sudoers
sed -i 's/^# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' /etc/sudoers
usermod -aG wheel $username

# ------------------------------------------------------
# Copy installation scripts to home directory 
# ------------------------------------------------------
mkdir -p /home/$username/.local/bin
cp /archinstall/* /home/$username/.local/bin/
chown -cR $username:$username /home/$username/.local/bin

clear
echo "     _                   "
echo "  __| | ___  _ __   ___  "
echo " / _' |/ _ \| '_ \ / _ \ "
echo "| (_| | (_) | | | |  __/ "
echo " \__,_|\___/|_| |_|\___| "
echo "                         "
echo ""
sleep 5
echo ""
echo "     System..."
echo ""
echo "       Rebooting......................."
echo ""
echo "          Now........."
echo ""
echo ""
sleep 2
reboot
